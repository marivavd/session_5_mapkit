class ModelPlatform {
  final String id;
  final String icon;
  bool choosen;
  final String title;
  String channels;
  final String availableChannels;

  ModelPlatform(
      {
        required this.id,
        required this.icon,
        this.choosen = false,
        required this.title,
        required this.channels,
        required this.availableChannels
      }
      );


  static ModelPlatform fromJson(Map<String, dynamic> json){
    return ModelPlatform(
        id: json["id"],
        icon: json["icon"],
        choosen: false,
        title: json["title"],
        channels: json["default_channels"],
        availableChannels: json["allow_channels"]
    );
  }

  Map<String, dynamic> toJson(){
    return {
      "id": id,
      "icon": icon,
      "choosen": false,
      "title": title,
      "default_channels": channels,
      "allow_channels": availableChannels
    };
  }
}