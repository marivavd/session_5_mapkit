import 'package:session_5_mapkit/data/models/model_platform.dart';

class ModelProfile{
  String fullname;
  var avatar;
  String phone;
  String birthday;
  List<ModelPlatform> platforms;


  ModelProfile(
      {
        required this.fullname,
        this.avatar,
        required this.phone,
        required this.birthday,
        required this.platforms
      }
      );


}