import 'package:session_5_mapkit/data/models/model_auth.dart';
import 'package:session_5_mapkit/data/models/model_platform.dart';

import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signIn(
    ModelAuth modelAuth,
    ) async {
  return await supabase.auth.signInWithPassword(
      email: modelAuth.email,
      password: modelAuth.password
  );
}
Map<String, dynamic> get_profile_atributes() {
  return supabase.auth.currentUser!.userMetadata!;
}


List<ModelPlatform> loadPlatforms() {
  List<dynamic> shortData = supabase.auth.currentUser!.userMetadata!["platforms"];

  List<ModelPlatform> platforms = shortData.map((e) =>
      ModelPlatform.fromJson(e)
  ).toList();

  return platforms;
}

Future<dynamic> get_my_avatar()async{
  if (supabase.auth.currentUser!.userMetadata!["avatar"] != null){
    var file = await supabase
        .storage
        .from('avatars')
        .download(supabase.auth.currentUser!.userMetadata!["avatar"]);
    return file;
  }
  return null;

}

Future<List<dynamic>> get_new_with_coords() async {
  final data = await supabase
      .from('news')
      .select()
      .neq('geo_lat', 0);
  return data;
}