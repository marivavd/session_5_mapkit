import 'package:session_5_mapkit/data/repository/requests.dart';
import 'package:session_5_mapkit/domain/map_point.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

Future<List<MapPoint>> getMapPoints()async{
  List<dynamic> data = [];
  List<MapPoint> result = [];
  final dataold = await get_new_with_coords();
  data = dataold;
  for (int i = 0; i < data.length; i++){
    result.add(MapPoint(name: data[i]['id'], latitude: double.tryParse(data[i]["geo_lat"].toString())??0.0, longitude: double.tryParse(data[i]["geo_long"].toString())??0.0),);
  }
  return result;
}
