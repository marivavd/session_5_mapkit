import 'package:session_5_mapkit/data/models/model_profile.dart';
import 'package:session_5_mapkit/data/repository/requests.dart';

Future<ModelProfile> make_profile()async{
  Map<String, dynamic> metadata = get_profile_atributes();
  return ModelProfile(fullname: metadata['fullname'], phone: metadata['phone'], birthday: metadata["birthday"], platforms: loadPlatforms(), avatar: await get_my_avatar());
}