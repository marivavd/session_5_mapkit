import 'package:session_5_mapkit/data/models/model_auth.dart';
import 'package:session_5_mapkit/data/repository/requests.dart';
import 'package:session_5_mapkit/domain/utils.dart';

class SignInUseCase {

  Future<void> pressButtonSignIn(
      String email,
      String password,
      Function(void) onResponse,
      Future<void> Function(String) onError
      ) async {
    requestSignIn() async {
      await signIn(ModelAuth(email: email, password: password));
    }
    await request(requestSignIn, onResponse, onError);
  }
}