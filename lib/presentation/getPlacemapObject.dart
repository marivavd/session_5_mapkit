import 'package:session_5_mapkit/domain/map_point.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:session_5_mapkit/domain/make_list_points.dart';


Future<ClusterizedPlacemarkCollection> getPlacemarkObject() async{
  List<MapPoint> sp = [];
  List<PlacemarkMapObject> result = [];
  final old_sp = await getMapPoints();
  sp = old_sp;
  for (int i = 0; i< sp.length; i++){
    result.add(PlacemarkMapObject(
        mapId: MapObjectId(sp[i].name),
        point: Point(latitude: sp[i].latitude, longitude: sp[i].longitude),
        opacity: 1,
        icon: PlacemarkIcon.single(
         PlacemarkIconStyle(
             image: BitmapDescriptor.fromAssetImage(
                 "assets/point_blue.png"
             ),
            scale: 2
          ),

        ),
    ));
  }
  final cluster = ClusterizedPlacemarkCollection(
      mapId: MapObjectId("cluster"),
      placemarks: result,
      radius: 30,
      minZoom: 15);
  return cluster;

}


