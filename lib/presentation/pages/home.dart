import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:session_5_mapkit/data/models/model_profile.dart';
import 'package:session_5_mapkit/domain/make_profile.dart';
import 'package:session_5_mapkit/domain/utils.dart';
import 'package:session_5_mapkit/main.dart';
import 'package:session_5_mapkit/presentation/pages/map.dart';



class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int index = 0;

  ModelProfile? profile;

  @override
  void initState(){
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
        profile = await make_profile();
        setState(() {});


    }
    );
  }


  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: (profile != null)?[Scaffold(backgroundColor: colors.background), Coords_Page(), Scaffold(backgroundColor: colors.background,), Scaffold(backgroundColor: colors.background,)][index]:Center(child: CircularProgressIndicator(),),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index,
        onTap: (val){
          setState(() {
            index = val;
          });
        },
        backgroundColor: colors.background,
        selectedItemColor: colors.accent,
        iconSize: 24,
        selectedFontSize: 12,
        showUnselectedLabels: true,
        showSelectedLabels: true,
        unselectedItemColor: colors.text,
        unselectedFontSize: 12,
        selectedLabelStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
        unselectedLabelStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: SvgPicture.asset("assets/home.svg", color: (index == 0)?colors.accent:colors.text,), label: "Home"),
          BottomNavigationBarItem(icon: SvgPicture.asset("assets/search.svg", color: (index == 1)?colors.accent:colors.text,), label: "Search"),
          BottomNavigationBarItem(icon: SvgPicture.asset("assets/favourite.svg", color: (index == 2)?colors.accent:colors.text,), label: "Favourite"),
          BottomNavigationBarItem(icon: SvgPicture.asset("assets/profile.svg", color: (index == 3)?colors.accent:colors.text,), label: "Profile"),
        ],
      ),
    );
  }
}