import 'package:flutter/material.dart';
import 'package:session_5_mapkit/data/repository/requests.dart';
import 'package:session_5_mapkit/domain/controllers/password_controller.dart';
import 'package:session_5_mapkit/domain/sign_in_use_case.dart';
import 'package:session_5_mapkit/domain/utils.dart';
import 'package:session_5_mapkit/main.dart';
import 'package:session_5_mapkit/presentation/pages/home.dart';
import 'package:session_5_mapkit/presentation/utils.dart';
import 'package:session_5_mapkit/presentation/widgets/custom_field.dart';


class LogInPage extends StatefulWidget {
  const LogInPage({super.key});

  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {

  TextEditingController email = TextEditingController();
  var password = PasswordController();
  bool rememberPassword = false;

  SignInUseCase useCase = SignInUseCase();

  bool isValidEmail = false;
  bool isRedEmail = false;
  bool isRedPassword = false;
  bool isValidPassword = false;
  bool isValid = false;


  void onChange(_){
    setState(() {
      isValidEmail = checkEmail(email.text);
      isRedEmail = !isValidEmail && email.text.isNotEmpty;
      isValidPassword = checkPassword(password.text);
      isRedPassword = !isValidPassword && password.text.isNotEmpty;
      isValid = isValidEmail && isValidPassword;
    });
  }


  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    var textTheme = Theme.of(context).textTheme;

    return Scaffold(
      backgroundColor: colors.background,
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83),
            Text(
              "Добро пожаловать",
              style: Theme.of(context).textTheme.titleLarge!.copyWith(
                  color: colors.text
              ),
            ),
            const SizedBox(height: 8),
            Text(
                "Заполните почту и пароль чтобы продолжить",
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: colors.subText
                )
            ),
            const SizedBox(height: 28),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email,
                isValid: !isRedEmail,
                onChange: onChange
            ),
            const SizedBox(height: 24),
            CustomTextField(
                label: "Пароль",
                hint: "**********",
                controller: password,
                enableObscure: true,
                isValid: !isRedPassword,
                onChange: onChange
            ),
            const SizedBox(height: 18),
            Row(
              children: [
                SizedBox.square(
                  dimension: 22,
                  child: Transform.scale(
                    scale: 1.2,
                    child: Checkbox(
                      value: rememberPassword,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)
                      ),
                      onChanged: (newValue) {
                        setState(() {
                          rememberPassword = newValue!;
                        });
                      },
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                    child: Text(
                        "Запомнить меня",
                        style: textTheme.titleMedium?.copyWith(
                            color: colors.subText
                        )
                    )
                ),
                GestureDetector(
                  onTap: (){},
                  child: Text(
                      "Забыли пароль?",
                      style: textTheme.titleMedium?.copyWith(
                          color: colors.accent
                      )
                  ),
                )
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                        onPressed: (isValid)
                            ? (){
                          showLoading(context);
                          useCase.pressButtonSignIn(
                              email.text,
                              password.hash_pass(),
                                  (_){
                                hideLoading(context);
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Home())).then((value) => setState(() {

                                }));
                              },
                                  (error) async {
                                hideLoading(context);
                                await showError(context, error);
                              }
                          );
                        } : null,

                        child: Text("Войти", style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color: colors.textAccent
                        ))
                    ),
                  ),
                  const SizedBox(height: 14),
                  GestureDetector(
                    onTap: (){},
                    child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "У меня нет аккаунта! ",
                              style: TextStyle(fontSize: 14, color: colors.subText)
                          ),
                          TextSpan(
                              text: "Cоздать",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: colors.accent,
                                  fontWeight: FontWeight.w700
                              )
                          ),
                        ]
                    )),
                  ),
                  const SizedBox(height: 34),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}