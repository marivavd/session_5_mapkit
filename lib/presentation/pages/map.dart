import 'dart:async';
import 'package:session_5_mapkit/domain/utils.dart';
import 'package:session_5_mapkit/presentation/getPlacemapObject.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:flutter/material.dart';

class Coords_Page extends StatefulWidget {
  const Coords_Page({super.key});




  @override
  State<Coords_Page> createState() => _Coords_PageState();
}

class _Coords_PageState extends State<Coords_Page> {
  Completer<YandexMapController> _completer = Completer();
  List<ClusterizedPlacemarkCollection> cluster = [];



  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{



    }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: YandexMap(
        zoomGesturesEnabled: true,
        onMapCreated: _onMapCreated,
        mapObjects: cluster,
      ),
    );
  }
  Future<void> _onMapCreated(YandexMapController controller)async{
    _completer.complete(controller);
    final cluster_old = await getPlacemarkObject();
    setState((){
      cluster = [cluster_old];
    });


  }


}

