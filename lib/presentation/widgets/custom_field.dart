import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:session_5_mapkit/main.dart';


class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isValid;
  final Function(String)? onChange;

  const CustomTextField(
      {
        super.key,
        required this.label,
        required this.hint,
        required this.controller,
        this.enableObscure = false,
        this.onChange,
        this.isValid = true
      }
      );

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  bool isObscure = true;

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    var theme = Theme.of(context).inputDecorationTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: colors.subText
          ),
        ),
        const SizedBox(height: 8),
        SizedBox(
          height: 44,
          child: TextField(
            obscureText: (widget.enableObscure) ? isObscure : false,
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: colors.text,
            ),
            obscuringCharacter: "*",
            controller: widget.controller,
            onChanged: widget.onChange,
            decoration: InputDecoration(
              enabledBorder: (widget.isValid) ? theme.enabledBorder : theme.errorBorder,
              focusedBorder: (widget.isValid) ? theme.focusedBorder : theme.errorBorder,
              contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleMedium?.copyWith(
                  color: colors.hint
              ),
              suffixIconConstraints: const BoxConstraints(minWidth: 34),
              suffixIcon: (widget.enableObscure)
                  ? GestureDetector(
                onTap: (){
                  setState(() {
                    isObscure = !isObscure;
                  });
                },
                child: SvgPicture.asset(
                    (!isObscure)
                        ? "assets/eye_open.svg"
                        : "assets/eye_close.svg",
                    colorFilter: ColorFilter.mode(colors.iconTint, BlendMode.color)
                ),
              )
                  : null,
            ),
          ),
        ),
      ],
    );
  }
}
